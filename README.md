# demanda-produtos-gbimbo

Projeto desenvolvido em R, como metodologia de aprendizado para um curso da Formação Cientista de Dados da Data Science Academy.

Trata-se do 2º projeto com feedback.

O objetivo do modelo é prever as demandas de produtos do Grupo Bimbo (https://www.grupobimbo.com).

O projeto está dividido em 2 etapas, nesta primeira o script foi desenvolvido em R para análise exploratória e plotagem dos dados.

A segunda parte foi desenvolvida no Microsoft Azure Machine Learning, onde foi construído um modelo para predição da demana dos produtos.



Fonte dos dados: https://www.kaggle.com/c/grupo-bimbo-inventory-demand